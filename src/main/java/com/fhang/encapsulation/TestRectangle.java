/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.encapsulation;

/**
 *
 * @author Natthakritta
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle  Rectangle1 =new  Rectangle(3,4);
        System.out.println("Area of Rectangle is "+Rectangle1.calArea());
        Rectangle1.setR(5,9);
        System.out.println("Area of Rectangle is "+Rectangle1.calArea());
        Rectangle1.setR(0,5);
        System.out.println("Area of Rectangle is "+Rectangle1.calArea());
        Rectangle1.setR(5,0);
        System.out.println("Area of Rectangle is "+Rectangle1.calArea());
        Rectangle1.setR(5,5);
        System.out.println("Area of Rectangle is "+Rectangle1.calArea());
    }
    
}
