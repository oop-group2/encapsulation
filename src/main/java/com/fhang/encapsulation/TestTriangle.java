/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.encapsulation;

/**
 *
 * @author Natthakritta
 */
public class TestTriangle {
    public static void main(String[] args) {
         Triangle Triangle1 = new Triangle(3,4);
         System.out.println("Area of Triangle is "+Triangle1.calArea());
         Triangle1.setR(4,-4);
         System.out.println("Area of Triangle is "+Triangle1.calArea());
         Triangle1.setR(0,0);
         System.out.println("Area of Triangle is "+Triangle1.calArea());
         Triangle1.setR(4,4);
         System.out.println("Area of Triangle is "+Triangle1.calArea());
    }
   
    
}
