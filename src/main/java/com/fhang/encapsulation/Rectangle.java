/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.encapsulation;

/**
 *
 * @author Natthakritta
 */
public class Rectangle {
    private double h ;
    private double w ;
    public Rectangle(double h,double w){
        this.h = h;
        this.w = w;
    }
    public double calArea(){
        return h*w;
    }
    public void setR(double h,double w){
        if (h==w) {
            System.out.println("ERROR!!!!!");
        }
        else if (h <= 0 || w <= 0) {
            System.out.println("ERROR!!!!!");
            return;
        }
        this.h = h;
        this.w = w;
    }
}
