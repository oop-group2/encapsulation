/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.encapsulation;

/**
 *
 * @author Natthakritta
 */
public class Triangle {
     private double h ;
     private double b ;
     public Triangle(double h,double b){
        this.h = h;
        this.b = b;
    }
    public double calArea(){
        return (h*b)/2;
    }
    public void setR(double h,double b){
        if (h<=0 || b<=0) {
            System.out.println("ERROR!!!!!");
            return;
        }
        this.h = h;
        this.b = b;
    }
}
