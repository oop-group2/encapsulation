/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.encapsulation;

/**
 *
 * @author Natthakritta
 */
public class TestSquare {
    public static void main(String[] args) {
        Square Square1 =new Square(2);
        System.out.println("Area of Square is "+Square1.calArea());
        Square1.setR(-1);
        System.out.println("Area of Square is "+Square1.calArea());
        Square1.setR(0);
        System.out.println("Area of Square is "+Square1.calArea());
    }
}
